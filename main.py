from gtts import gTTS
from tkinter import Tk
from tkinter.filedialog import askopenfilename
import PyPDF2

Tk().withdraw()
openfile = askopenfilename()

text = ''

pdfFileObj = open(openfile, 'rb')
pdfReader = PyPDF2.PdfReader(pdfFileObj)

for i in range(0, len(pdfReader.pages)):
    pageObj = pdfReader.pages[i]
    text += pageObj.extract_text()

name = text.split()[:3]
tts = gTTS(text=text,lang='pl')
tts.save(f"{'_'.join(name)}(Audio)_PDF.mp3")

pdfFileObj.close()



